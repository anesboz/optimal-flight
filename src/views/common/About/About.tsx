import { Grid } from '@mui/material'
import { APP_NAME } from 'src/common/constants'

export default function About() {
  return (
    <Grid container sx={{ margin: `auto`, m: 10 }} className="center">
      <b>{APP_NAME}</b>&nbsp;
      {` est une application en React TypeScript`}
    </Grid>
  )
}
