import {
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Grid,
} from '@mui/material'
// confirmation box
import store from 'src/common/redux/store'
import { mainActions } from 'src/common/redux/reducers/main'
import List from '@mui/material/List'
import MIcon from 'src/components/MIcon'

export default function Setting() {
  const openDialog = (elment: JSX.Element) =>
    store.dispatch(mainActions.openDialog(elment))

  return (
    <Grid container className="center" sx={{ p: 5 }}>
      <List sx={{ width: '100%' }}>
        <ListItem onClick={() => {}} disablePadding>
          <ListItemButton>
            <ListItemIcon>
              <MIcon name={`DeleteForever`} />
            </ListItemIcon>
            <ListItemText className="center-x" primary={`Supprimer le cache`} />
          </ListItemButton>
        </ListItem>
      </List>
    </Grid>
  )
}
