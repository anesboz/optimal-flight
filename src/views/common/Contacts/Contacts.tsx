import { Grid, Typography } from '@mui/material'
import { APP_NAME_FREINDLY, ENV_METADATA } from 'src/common/constants'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Divider from '@mui/material/Divider'
import MIcon from 'src/components/MIcon'

const contact = {
  email: `anesbouzouaoui@gmail.com`,
}

export default function Contacts() {
  const { version, branch } = ENV_METADATA
  return (
    <Grid container className="center">
      <Grid container className="center-x">
        <Typography variant="h5" sx={{ m: 5 }}>
          {APP_NAME_FREINDLY}
        </Typography>
      </Grid>
      <Box sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
        <nav aria-label="main mailbox folders">
          <List>
            <ListItem disablePadding>
              <ListItemButton
                onClick={(e) => {
                  window.location.href = 'mailto:' + contact.email
                  e.preventDefault()
                }}
              >
                <ListItemIcon>
                  <MIcon name={`Drafts`} fontSize={undefined} />
                </ListItemIcon>
                <ListItemText primary="Send an email to support" />
              </ListItemButton>
            </ListItem>
          </List>
        </nav>
        <Divider />
        <nav aria-label="secondary mailbox folders">
          <List>
            <ListItem disablePadding>
              <ListItemButton disabled>
                <ListItemText primary={`Version : ${version} (${branch})`} />
              </ListItemButton>
            </ListItem>
          </List>
        </nav>
      </Box>
    </Grid>
  )
}
