import { LoadingButton } from '@mui/lab'
import { Grid } from '@mui/material'
import avionService from 'src/services/amadeus/avion.service'
import { DataGrid } from '@mui/x-data-grid'
import { useState } from 'react'

export default function Main() {
  const [rows, setRows] = useState<any[]>([])
  const sample = rows[0]
  const columns = Object.keys(sample ?? {}).map((field) => ({
    field,
    width: ('' + sample[field]).length * 10 + 20,
  }))
  const [loading, setLoading] = useState(false)

  return (
    <Grid container className="center">
      <LoadingButton
        fullWidth
        variant="contained"
        color={'success'}
        loading={loading}
        onClick={async () => {
          setLoading(true)
          const accessToken = await avionService.getAmadeusKey()
          const date = '2023-09-16'
          const res = await avionService.getFlights(accessToken, date)
          const printable = await avionService.displayFlights(accessToken, res)
          setRows(printable)
          setLoading(false)
        }}
      >
        Explore
      </LoadingButton>
      <DataGrid rows={rows} columns={columns} />
    </Grid>
  )
}
