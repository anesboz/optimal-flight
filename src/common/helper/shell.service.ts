export enum FileFormat {
  XLSX = 'xlsx',
  JSON = 'json',
  CSV = 'csv',
  TXT = 'txt',
}

export enum ENVS {
  MASTER = `master`,
  INTEGRATION = `integration`,
  DEVELOP = `develop`,
}

class ShellService {
  getENVmetadata(appName: string) {
    const { VITE_ENV, VITE_VERSION } = process.env
    const version = VITE_VERSION ?? 'LIVE'
    const isDev = VITE_ENV != ENVS.MASTER && VITE_ENV != ENVS.INTEGRATION
    const branch = isDev ? ENVS.DEVELOP : VITE_ENV

    return {
      appName,
      branch,
      version,
    }
  }
}

export default new ShellService()
