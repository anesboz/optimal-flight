import shellService from './helper/shell.service'

export const APP_NAME = process.env.VITE_APP_NAME ?? ''
export const APP_NAME_FREINDLY =
  APP_NAME.charAt(0).toUpperCase() + APP_NAME.slice(1)
export const APP_BASENAME = `/` + APP_NAME

export const ENV_METADATA = shellService.getENVmetadata(APP_NAME)
export const ISO_DATE = 'YYYY-MM-DD'

export interface MObject {
  [key: string]: any
}


export const AMADEUS_API_URL = 'https://test.api.amadeus.com'