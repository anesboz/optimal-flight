import { createSlice, PayloadAction } from '@reduxjs/toolkit'

// Define a type for the slice state
interface MainState {
  value: number
  drawer: IDrawer
  dialog: IDialog
}

// Define the initial state using that type
const initialState: MainState = {
  value: 0,
  drawer: {
    open: false,
    element: null,
  },
  dialog: {
    timestamp: 0,
    element: null,
  },
}

export const mainSlice = createSlice({
  name: 'main',
  initialState,
  reducers: {
    setDrawerElement: (state, action: PayloadAction<JSX.Element>) => {
      state.drawer.element = action.payload
    },
    openDrawer: (state, action: PayloadAction<boolean>) => {
      state.drawer.open = action.payload
    },
    // setDialogElement: (state, action: PayloadAction<JSX.Element>) => {
    //   state.dialog.element = action.payload
    // },
    openDialog: (state, action: PayloadAction<JSX.Element | null>) => {
      state.dialog = {
        timestamp: new Date().getTime(),
        element: action.payload,
      }
    },
    closeDialog: (state, action: PayloadAction<void>) => {
      state.dialog.timestamp = 0
    },
  },
})

interface IDrawer {
  open: boolean
  element: JSX.Element | null
}
interface IDialog {
  timestamp: number
  element: JSX.Element | null
}
// Action creators are generated for each case reducer function
export const mainActions = mainSlice.actions

export const mainReducer = mainSlice.reducer
