import * as React from 'react'
import Box from '@mui/material/Box'
import SpeedDial from '@mui/material/SpeedDial'
import SpeedDialIcon from '@mui/material/SpeedDialIcon'
import SpeedDialAction from '@mui/material/SpeedDialAction'
import { Icon } from '@mui/material'
import MIcon from '../MIcon'

interface Action {
  name: string
  action: () => any
}

type Position = 'right' | 'left'

export default function SpeedDialM(props: {
  actions: Action[]
  icon: string
  pos: Position
}) {
  const { actions, icon, pos } = props
  const [open, setOpen] = React.useState(false)
  return (
    <SpeedDial
      sx={{ position: 'absolute', bottom: 120, [pos]: 100 }}
      ariaLabel={Math.random() * 10000 + ''}
      open={open}
      onClick={() => setOpen(!open)}
      icon={
        <SpeedDialIcon
          openIcon={<MIcon name={icon} />}
          icon={<MIcon name={icon} />}
        />
      }
    >
      {actions.map(({ name, action }, i) => {
        return (
          <SpeedDialAction
            key={i}
            icon={<MIcon name={`Bolt`} />}
            tooltipTitle={name}
            tooltipOpen
            onClick={() => {
              console.log(`${name} action triggered`)
              action()
            }}
          />
        )
      })}
    </SpeedDial>
  )
}
