import { IconTypeMap } from '@mui/material'
import * as MUIcon from '@mui/icons-material'

type IconProps = IconTypeMap['props']

interface MIconProps extends IconProps {
  name: string | undefined
}

export default function MIcon(props: MIconProps | any) {
  const { name } = props
  const Icon = MUIcon[name as keyof typeof MUIcon]
  if (Icon == null) {
    const message = `Costom Error : There is no "${name}" Icon`
    console.log(message)
    return null
  }
  return <Icon {...props} />
  // return <Icon fontSize="inherit" {...props} />
}
