import * as React from 'react'
import Box from '@mui/material/Box'
import FormLabel from '@mui/material/FormLabel'
import FormControl from '@mui/material/FormControl'
import FormGroup from '@mui/material/FormGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormHelperText from '@mui/material/FormHelperText'
import Checkbox from '@mui/material/Checkbox'

interface ICheck {
  title: string
  hook: [state: any, setState: (newState: any) => void]
  horizontal?: boolean
}

export default function CheckboxesGroup(props: ICheck) {
  const { title, hook, horizontal } = props
  const [state, setState] = hook

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({
      ...state,
      [event.target.name]: event.target.checked,
    })
  }

  const style =
    horizontal === true
      ? {
          position: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }
      : {}

  return (
    <Box sx={{ display: 'flex' }}>
      <FormControl component="fieldset" variant="standard">
        <FormLabel component="legend">{title}</FormLabel>
        <FormGroup sx={style}>
          {Object.entries(state).map((arg, i) => {
            const [key, value]: [string, any] = arg
            return (
              <FormControlLabel
                key={i}
                control={
                  <Checkbox
                    checked={value}
                    onChange={handleChange}
                    name={key}
                  />
                }
                label={key}
              />
            )
          })}
        </FormGroup>
        {/* <FormHelperText>Be careful</FormHelperText> */}
      </FormControl>
    </Box>
  )
}
