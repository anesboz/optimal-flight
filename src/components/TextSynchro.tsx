import { TextField } from '@mui/material'
import { useEffect, useState } from 'react'

export default function TextFieldSynchro(props: {
  value: string
  callback: (newValue: string) => void
  placeholder?: string
}) {
  const { value, callback, placeholder } = props
  const [localchanges, setLocalchanges] = useState('')

  useEffect(() => {
    setLocalchanges(value)
  }, [value])

  return (
    <TextField
      variant="standard"
      InputProps={{
        disableUnderline: true,
        inputProps: {
          style: { textAlign: 'center' },
        },
      }}
      value={localchanges}
      onChange={(event: any) => setLocalchanges(event.target.value)}
      onBlur={() => callback(localchanges)}
      // sx={{ input: { color: '#4f504e5e' } }}
      placeholder={placeholder ?? 'Add text here ...'}
    />
  )
}
