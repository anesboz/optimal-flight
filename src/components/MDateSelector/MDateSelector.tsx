import { useEffect, useState } from 'react'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker'
import frLocale from 'date-fns/locale/fr'
import { FormHelperText, Grid } from '@mui/material'
import { Box } from '@mui/system'

export type ISetState = (payload: IPayload) => void
type IPayload = {
  start: Date
  end: Date
}

export default function MDateSelector(props: {
  setState: ISetState
  disabled?: boolean
  error?: boolean
  helperText?: string
}) {
  const { setState, disabled, error, helperText } = props
  const [start, setStart] = useState(new Date())
  const [end, setEnd] = useState(new Date())

  useEffect(() => {
    setState({ start, end })
  }, [start, end])

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={frLocale}>
      <Grid container justifyContent="center">
        <Grid item xs={6} className="center-x">
          <MobileDatePicker
            views={['year', 'month', 'day']}
            label="Start"
            value={start}
            onChange={(value) => {
              if (value == null) {
                return
              }
              setStart(value)
              setEnd(value)
            }}
            disableFuture
            sx={{ cursor: `pointer`, mx: 2 }}
            disabled={disabled}
            // maxDate={end}
          />
        </Grid>
        <Grid item xs={6} className="center-x">
          <MobileDatePicker
            label="End"
            value={end}
            onChange={(value) => value && setEnd(value)}
            disableFuture
            disabled={disabled}
            minDate={start}
          />
        </Grid>
        <Grid container>
          <FormHelperText
            sx={{
              color: 'red',
              p: 1,
              ml: 2,
              display: error === true ? '' : 'none',
            }}
          >
            {`Error : ${helperText}`}
          </FormHelperText>
        </Grid>
      </Grid>
    </LocalizationProvider>
  )
}
