import * as React from 'react'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
interface DevButton {
  label: string
  onClick: () => void
}

export default function DevButtons(props: { devButtonsList: DevButton[] }) {
  const { devButtonsList } = props
  // display only in development mode
  // if (process.env.NODE_ENV === 'production') {
  //   return null
  // }
  return (
    <Box className="devButton red">
      <div
        style={{ width: `100%`, padding: 2, backgroundColor: `#4ccdc4` }}
        className=""
      >
        DEV
      </div>
      <nav>
        <List className="">
          {devButtonsList.map(({ label, onClick }, i) => (
            <ListItem disablePadding key={i} className="">
              <ListItemButton onClick={onClick} className="">
                <ListItemText primary={label} className="" />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </nav>
    </Box>
  )
}
