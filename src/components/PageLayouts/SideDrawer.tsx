import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import {
  Divider,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material'
import { routes, Tags } from 'src/routes'
import store, { useAppSelector } from 'src/common/redux/store'
import { mainActions } from 'src/common/redux/reducers/main'
import { useNavigate } from 'react-router-dom'
import MIcon from 'src/components/MIcon'
import { APP_NAME_FREINDLY } from 'src/common/constants'

const DRAWER_WIDTH = 240

export default function SideDrawer() {
  const { open, element } = useAppSelector((state) => state.main.drawer)
  const toggleDrawer = () => store.dispatch(mainActions.openDrawer(!open))

  const navigate = useNavigate()

  return (
    <Box component="nav">
      <Drawer
        variant="temporary"
        open={open}
        onClose={toggleDrawer}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: 'block' },
          '& .MuiDrawer-paper': {
            boxSizing: 'border-box',
            width: DRAWER_WIDTH,
          },
        }}
      >
        <ListItem disablePadding>
          <ListItemButton
            onClick={() => {
              navigate('/')
              toggleDrawer()
            }}
          >
            <ListItemIcon className="center-y">
              <MIcon name={`Home`} />
              &nbsp;&nbsp;
              {APP_NAME_FREINDLY}
            </ListItemIcon>
            <ListItemText />
          </ListItemButton>
        </ListItem>
        <Divider />
        <Box>
          <List>
            {routes
              .filter((e) => e.tags.includes(Tags.SIDE_MENU))
              .map(({ name, path, icon }) => {
                return (
                  <ListItem
                    key={name}
                    disablePadding
                    onClick={() => {
                      navigate(path)
                      toggleDrawer()
                    }}
                  >
                    <ListItemButton>
                      <ListItemIcon>
                        <MIcon name={icon} color={'secondary'} />
                      </ListItemIcon>
                      <ListItemText primary={name} />
                    </ListItemButton>
                  </ListItem>
                )
              })}
          </List>
          <Divider />
        </Box>
        {element}
      </Drawer>
    </Box>
  )
}
