import { Grid, IconButton } from '@mui/material'
import { useLocation, useNavigate } from 'react-router-dom'
import MIcon from '../MIcon'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import { AppBar } from '@mui/material'
import { routes, Tags } from 'src/routes'
import store, { useAppSelector } from 'src/common/redux/store'
import { mainActions } from 'src/common/redux/reducers/main'

export default function TopBar() {
  const location = useLocation()
  const navigate = useNavigate()
  const route = routes.find((e) => e.path === location.pathname)
  const isHomePage = route?.path === '/'
  const { open } = useAppSelector((state) => state.main.drawer)
  const toggleDrawer = () => store.dispatch(mainActions.openDrawer(!open))

  const PAD = 20

  return (
    <Grid container sx={{ mb: 5 }}>
      <AppBar component="nav" position="relative">
        <Toolbar>
          <Box
            sx={{
              position: `absolute`,
              left: PAD,
            }}
          >
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={toggleDrawer}
            >
              <MIcon name={'Menu'} />
            </IconButton>

            <IconButton
              color="inherit"
              edge="start"
              onClick={() => {
                if (isHomePage) {
                  return navigate(0)
                }
                navigate('/')
              }}
              sx={{ p: 3 }}
              className="hover"
              size="small"
            >
              <MIcon name={'Home'} />
              &nbsp;
              {`Home`}
            </IconButton>
          </Box>

          <Button
            sx={{ color: `white`, margin: `auto` }}
            onClick={() => {
              navigate(0)
              // if (isHomePage === true) {
              //   return navigate(0)
              // }
              // navigate('/')
            }}
            startIcon={<MIcon name={route?.icon} />}
          >
            <Typography
              variant="h6"
              component="span"
              sx={{ cursor: `pointer` }}
            >
              {route?.name}
            </Typography>
          </Button>

          <Box
            sx={{
              position: `absolute`,
              right: PAD,
            }}
          >
            {routes
              .filter((e) => e.tags.includes(Tags.TOP_MENU))
              .map(({ name, path, icon }) => {
                return (
                  <IconButton
                    key={name}
                    color="inherit"
                    aria-label="open drawer"
                    edge="end"
                    onClick={() => navigate(path)}
                  >
                    <MIcon name={icon} />
                  </IconButton>
                )
              })}
          </Box>
        </Toolbar>
      </AppBar>
      <Grid container sx={{ position: `relative` }}>
        <IconButton
          edge="start"
          onClick={() => navigate(-1)}
          disabled={isHomePage}
          sx={{
            p: 3,
            ml: 1,
            display: isHomePage ? `none` : '',
            color: `gray`,
            position: `absolute`,
          }}
        >
          <MIcon name={'ArrowBack'} />
        </IconButton>
      </Grid>
    </Grid>
  )
}
