import * as React from 'react'
import LinearProgress from '@mui/material/LinearProgress'
import Typography from '@mui/material/Typography'
import { Grid } from '@mui/material'

interface IProps {
  loading: boolean
  value?: number
}
export default function MProgress(props: IProps) {
  const { loading, value } = props

  const [progress, setProgress] = React.useState(0)

  React.useEffect(() => {
    if (value != null) {
      setProgress(value)
      return
    }
    const timer = setInterval(() => {
      setProgress((oldProgress) => {
        const majoration = Math.floor(Math.random() * 100)
        if (majoration > oldProgress) {
          return oldProgress + 1
        }
        return oldProgress
      })
    }, 500)

    return () => {
      clearInterval(timer)
    }
  }, [value])

  React.useEffect(() => {
    setProgress(0)
  }, [loading])

  return (
    <Grid
      container
      sx={{ visibility: loading ? 'visible' : 'hidden' }}
      className=""
    >
      <Grid item className="center" flex={1}>
        <LinearProgress
          variant="determinate"
          sx={{ width: '100%', mr: 1 }}
          color="info"
          value={progress}
        />
      </Grid>
      <Grid item >
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          progress
        )}%`}</Typography>
      </Grid>
    </Grid>
  )
}
