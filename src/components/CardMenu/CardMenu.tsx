import { Grid, Tooltip } from '@mui/material'
import { routes } from 'src/routes'
import { useNavigate } from 'react-router-dom'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import Typography from '@mui/material/Typography'
import { CardActionArea } from '@mui/material'
import MIcon from 'src/components/MIcon'

export default function CardMenu(props: { paths: string[] }) {
  const { paths } = props
  const navigate = useNavigate()

  const list = routes.filter((e) =>
    paths.some((reg) => new RegExp(`^${reg}$`).test(e.path))
  )

  return (
    <Grid
      container
      sx={{ maxWidth: 1000 }}
      columnSpacing={2}
      className="center-x"
      justifyContent="space-around"
    >
      {list.map((route, i) => {
        const { icon, name, description, path, image } = route

        const MAX_DESCRIPTION = 120
        const isLongMessage =
          description != null && description.length > MAX_DESCRIPTION

        return (
          <Grid item xs={4} flex={1} key={i}>
            <Card
              sx={{ width: `100%`, height: 300 }}
              onClick={() => navigate(path)}
            >
              <CardActionArea>
                <CardMedia component="img" height="140" image={image} />
                <CardContent>
                  <Typography
                    gutterBottom
                    variant="h5"
                    component="div"
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <MIcon sx={{ mr: 0.5 }} fontSize="inherit" name={icon} />
                    {name}
                  </Typography>
                  {description == null ? null : (
                    <Tooltip
                      title={description.slice(MAX_DESCRIPTION)}
                      disableHoverListener={!isLongMessage}
                    >
                      <Typography variant="body2" color="text.secondary">
                        {description.slice(0, MAX_DESCRIPTION)}
                        {isLongMessage ? '...' : ''}
                      </Typography>
                    </Tooltip>
                  )}
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        )
      })}
    </Grid>
  )
}
