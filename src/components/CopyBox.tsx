import IconButton from '@mui/material/IconButton'
import MIcon from 'src/components/MIcon'
import { useSnackbar } from 'notistack'

export default function CopyBox(props: { message: string; hide?: boolean }) {
  const { message, hide } = props

  const { enqueueSnackbar } = useSnackbar()
  return (
    <pre
      style={{
        position: `relative`,
        width: `100%`,
        display: hide ? 'none' : '',
        whiteSpace: `break-spaces`,
        wordBreak: `break-word`,
        padding: 2,
        margin: 0,
      }}
    >
      {message.trim()}
      <IconButton
        sx={{ position: `absolute`, right: 0, bottom: 0 }}
        onClick={() => {
          navigator.clipboard.writeText(message)
          enqueueSnackbar(`copied`)
        }}
      >
        <MIcon name={`ContentCopy`} />
      </IconButton>
    </pre>
  )
}
