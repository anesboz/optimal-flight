import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import { useAppSelector } from 'src/common/redux/store'
import { useEffect, useState } from 'react'

export default function MDialog() {
  const { dialog } = useAppSelector((state) => state.main)
  const { timestamp, element } = dialog
  const [localOpen, setLocallocalOpen] = useState(false)

  useEffect(() => {
    if (timestamp === 0) {
      return setLocallocalOpen(false)
    }
    setLocallocalOpen(true)
  }, [timestamp])

  const handleClose = () => setLocallocalOpen(false)
  return (
    <Dialog
      open={localOpen}
      onClose={handleClose}
      aria-labelledby="scroll-dialog-title"
      aria-describedby="scroll-dialog-description"
    >
      {/* <DialogTitle id="scroll-dialog-title">Dialog</DialogTitle> */}
      <DialogContent>{element}</DialogContent>
      {/* <DialogActions>
        <Button
          onClick={() => {
            setLocallocalOpen(false)
          }}
          color={'error'}
          variant={'outlined'}
        >
          OK
        </Button>
      </DialogActions> */}
    </Dialog>
  )
}
