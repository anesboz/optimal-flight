
import { Grid, Button } from '@mui/material'
import store, { useAppSelector } from 'src/common/redux/store'
import { mainActions } from 'src/common/redux/reducers/main'

export default function ConfirmBox(props: { action: () => (Promise<void> | void) }) {
  const { action } = props
  return (
    <Grid container>
      <Grid item xs={12}>
        <h3>Are you sure?</h3>
      </Grid>
      <Grid item xs={12}>
        Do you really want to procede ? This process cannot be undone.
      </Grid>
      <Grid item xs={12} className="center">
        <Button
          variant="contained"
          color="error"
          onClick={async () => {
            await action()
            store.dispatch(mainActions.closeDialog())
          }}
          sx={{ m: 3 }}
        >
          Confirm
        </Button>
      </Grid>
    </Grid>
  )
}