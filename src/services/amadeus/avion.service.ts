import axios from 'axios'
import { AMADEUS_API_URL, MObject } from 'src/common/constants'

class AvionService {
  async getAmadeusKey() {
    const client_id = process.env.VITE_AMADEUS_CLIENT_ID ?? ''
    const client_secret = process.env.VITE_AMADEUS_CLIENT_SECRET ?? ''
    const data = {
      grant_type: 'client_credentials',
      client_id,
      client_secret,
    }
    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }
    const apiUrl = AMADEUS_API_URL + '/v1/security/oauth2/token'
    try {
      const res = await axios.post(
        apiUrl,
        new URLSearchParams(data).toString(),
        config
      )
      const { access_token } = res.data
      const ret = access_token
      return ret
    } catch (error: any) {
      const message = error?.response?.data ?? error
      console.log(`message:`, message)
    }
  }

  async getFlights(accessToken: string, saturdayDate: string) {
    const requestData = {
      currencyCode: 'EUR',
      originDestinations: [
        {
          id: '1',
          originLocationCode: 'PAR',
          destinationLocationCode: 'ALG',
          departureDateTimeRange: {
            date: saturdayDate,
            time: '00:00:00',
            dateWindow: 'M2D',
          },
        },
      ],
      travelers: [
        {
          id: '1',
          travelerType: 'ADULT',
        },
      ],
      sources: ['GDS'],
      searchCriteria: {
        maxFlightOffers: 10,
        // maxPrice: 1000000,
        flightFilters: {
          // cabinRestrictions: [
          //   {
          //     cabin: 'BUSINESS',
          //     coverage: 'MOST_SEGMENTS',
          //     originDestinationIds: ['1'],
          //   },
          // ],
          connectionRestriction: {
            maxNumberOfConnections: 0,
            // nonStopPreferred: true,
          },
        },
      },
    }
    const config = {
      headers: {
        accept: 'application/vnd.amadeus+json',
        'Content-Type': 'application/vnd.amadeus+json',
        Authorization: `Bearer ${accessToken}`,
      },
    }
    const apiUrl = AMADEUS_API_URL + '/v2/shopping/flight-offers'

    try {
      const res = await axios.post(apiUrl, requestData, config)
      const data = res.data
      return data
    } catch (error: any) {
      const message = error?.response?.data ?? error
      console.log(`message:`, message)
    }
  }

  async displayFlights(accessToken: string, res: any) {
    const fligts = res.data
    const ret = []
    for (const flight of fligts) {
      const itinerary = flight.itineraries[0]
      const segments = itinerary.segments
      const firstSegment = segments[0]

      const [time_date, time_hour] = firstSegment.departure.at.split('T')
      const time_unix = new Date(firstSegment.departure.at).getTime()

      const airline = res.dictionaries.carriers[firstSegment.carrierCode]
      const airports = segments
        .map((segment: any) => {
          const tab = ['departure', 'arrival']
          return tab
            .map((e) => {
              const iataCode = segment[e].iataCode
              const cityCode = res.dictionaries.locations[iataCode].cityCode
              return `${iataCode} (${cityCode})`
            })
            .join(' > ')
        })
        .join(' >> ')
      const totalPrice = flight.price.total

      const element: MObject = {
        id: time_unix + '_' + airline,
        time_date,
        time_hour,
        totalPrice,
        airline,
        airports,
      }

      // // getting realPrice
      // const ress = await this.getPrices(accessToken, [flight])
      // console.log(`ress:`, ress)
      // const realPrice = ress.data.flightOffers[0].price.total
      // console.log(`realPrice:`, realPrice)
      // element.realPrice = realPrice

      ret.push(element)
    }

    return ret //.sort((a: any, b: any) => a.totalPrice - b.totalPrice)
  }

  private async getPrices(accessToken: string, flightOffers: any) {
    const payload = {
      data: {
        type: 'flight-offers-pricing',
        flightOffers: flightOffers.slice(0, 1),
      },
    }

    const config = {
      headers: {
        accept: 'application/vnd.amadeus+json',
        'Content-Type': 'application/vnd.amadeus+json',
        Authorization: `Bearer ${accessToken}`,
      },
    }
    const apiUrl = AMADEUS_API_URL + '/v1/shopping/flight-offers/pricing'

    try {
      const res = await axios.post(apiUrl, payload, config)
      const data = res.data
      return data
    } catch (error: any) {
      const message = error?.response?.data ?? error
      console.log(`message:`, message)
    }
  }
}

export default new AvionService()
