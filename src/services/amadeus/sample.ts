export const flightsSample = {
  meta: {
    count: 10,
  },
  data: [
    {
      type: 'flight-offer',
      id: '1',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-08',
      lastTicketingDateTime: '2023-09-08',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT5H5M',
          segments: [
            {
              departure: {
                iataCode: 'CDG',
                terminal: '2F',
                at: '2023-09-15T06:10:00',
              },
              arrival: {
                iataCode: 'FCO',
                terminal: '1',
                at: '2023-09-15T08:10:00',
              },
              carrierCode: 'AZ',
              number: '317',
              aircraft: {
                code: '320',
              },
              operating: {
                carrierCode: 'AZ',
              },
              duration: 'PT2H',
              id: '1',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
            {
              departure: {
                iataCode: 'FCO',
                terminal: '1',
                at: '2023-09-15T09:20:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T10:15:00',
              },
              carrierCode: 'AZ',
              number: '800',
              aircraft: {
                code: '32S',
              },
              operating: {
                carrierCode: 'AZ',
              },
              duration: 'PT1H55M',
              id: '2',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '108.47',
        base: '16.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '108.47',
        additionalServices: [
          {
            amount: '60.00',
            type: 'CHECKED_BAGS',
          },
        ],
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: false,
      },
      validatingAirlineCodes: ['AZ'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '108.47',
            base: '16.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '1',
              cabin: 'ECONOMY',
              fareBasis: 'OOLGEU1',
              brandedFare: 'ECOLIGHT',
              class: 'O',
              includedCheckedBags: {
                quantity: 0,
              },
            },
            {
              segmentId: '2',
              cabin: 'ECONOMY',
              fareBasis: 'OOLGEU1',
              brandedFare: 'ECOLIGHT',
              class: 'O',
              includedCheckedBags: {
                quantity: 0,
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '2',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-08',
      lastTicketingDateTime: '2023-09-08',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT17H55M',
          segments: [
            {
              departure: {
                iataCode: 'CDG',
                terminal: '2F',
                at: '2023-09-15T06:10:00',
              },
              arrival: {
                iataCode: 'FCO',
                terminal: '1',
                at: '2023-09-15T08:10:00',
              },
              carrierCode: 'AZ',
              number: '317',
              aircraft: {
                code: '320',
              },
              operating: {
                carrierCode: 'AZ',
              },
              duration: 'PT2H',
              id: '12',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
            {
              departure: {
                iataCode: 'FCO',
                terminal: '1',
                at: '2023-09-15T22:10:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T23:05:00',
              },
              carrierCode: 'AZ',
              number: '802',
              aircraft: {
                code: '32S',
              },
              operating: {
                carrierCode: 'AZ',
              },
              duration: 'PT1H55M',
              id: '13',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '108.47',
        base: '16.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '108.47',
        additionalServices: [
          {
            amount: '60.00',
            type: 'CHECKED_BAGS',
          },
        ],
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: false,
      },
      validatingAirlineCodes: ['AZ'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '108.47',
            base: '16.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '12',
              cabin: 'ECONOMY',
              fareBasis: 'OOLGEU1',
              brandedFare: 'ECOLIGHT',
              class: 'O',
              includedCheckedBags: {
                quantity: 0,
              },
            },
            {
              segmentId: '13',
              cabin: 'ECONOMY',
              fareBasis: 'OOLGEU1',
              brandedFare: 'ECOLIGHT',
              class: 'O',
              includedCheckedBags: {
                quantity: 0,
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '3',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-10',
      lastTicketingDateTime: '2023-09-10',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT2H25M',
          segments: [
            {
              departure: {
                iataCode: 'CDG',
                terminal: '3',
                at: '2023-09-15T07:45:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T09:10:00',
              },
              carrierCode: '5O',
              number: '271',
              aircraft: {
                code: '73W',
              },
              operating: {
                carrierCode: '5O',
              },
              duration: 'PT2H25M',
              id: '8',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '110.25',
        base: '48.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '110.25',
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: false,
      },
      validatingAirlineCodes: ['5O'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '110.25',
            base: '48.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '8',
              cabin: 'ECONOMY',
              fareBasis: 'VOWBASIC',
              brandedFare: 'BASIC',
              class: 'V',
              includedCheckedBags: {
                quantity: 0,
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '4',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-10',
      lastTicketingDateTime: '2023-09-10',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT2H25M',
          segments: [
            {
              departure: {
                iataCode: 'CDG',
                terminal: '3',
                at: '2023-09-15T21:25:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: '4',
                at: '2023-09-15T22:50:00',
              },
              carrierCode: '5O',
              number: '165',
              aircraft: {
                code: '73W',
              },
              operating: {
                carrierCode: '5O',
              },
              duration: 'PT2H25M',
              id: '9',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '110.25',
        base: '48.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '110.25',
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: false,
      },
      validatingAirlineCodes: ['5O'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '110.25',
            base: '48.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '9',
              cabin: 'ECONOMY',
              fareBasis: 'VOWBASIC',
              brandedFare: 'BASIC',
              class: 'V',
              includedCheckedBags: {
                quantity: 0,
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '5',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-10',
      lastTicketingDateTime: '2023-09-10',
      numberOfBookableSeats: 4,
      itineraries: [
        {
          duration: 'PT10H45M',
          segments: [
            {
              departure: {
                iataCode: 'CDG',
                terminal: '2F',
                at: '2023-09-15T13:20:00',
              },
              arrival: {
                iataCode: 'FCO',
                terminal: '1',
                at: '2023-09-15T15:25:00',
              },
              carrierCode: 'AZ',
              number: '333',
              aircraft: {
                code: '32S',
              },
              operating: {
                carrierCode: 'AZ',
              },
              duration: 'PT2H5M',
              id: '10',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
            {
              departure: {
                iataCode: 'FCO',
                terminal: '1',
                at: '2023-09-15T22:10:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T23:05:00',
              },
              carrierCode: 'AZ',
              number: '802',
              aircraft: {
                code: '32S',
              },
              operating: {
                carrierCode: 'AZ',
              },
              duration: 'PT1H55M',
              id: '11',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '128.47',
        base: '26.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '128.47',
        additionalServices: [
          {
            amount: '60.00',
            type: 'CHECKED_BAGS',
          },
        ],
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: false,
      },
      validatingAirlineCodes: ['AZ'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '128.47',
            base: '26.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '10',
              cabin: 'ECONOMY',
              fareBasis: 'LOLGEU1',
              brandedFare: 'ECOLIGHT',
              class: 'L',
              includedCheckedBags: {
                quantity: 0,
              },
            },
            {
              segmentId: '11',
              cabin: 'ECONOMY',
              fareBasis: 'LOLGEU1',
              brandedFare: 'ECOLIGHT',
              class: 'L',
              includedCheckedBags: {
                quantity: 0,
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '6',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-14',
      lastTicketingDateTime: '2023-09-14',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT2H10M',
          segments: [
            {
              departure: {
                iataCode: 'ORY',
                terminal: '4',
                at: '2023-09-15T07:40:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T08:50:00',
              },
              carrierCode: 'AH',
              number: '1009',
              aircraft: {
                code: '73H',
              },
              operating: {
                carrierCode: 'AH',
              },
              duration: 'PT2H10M',
              id: '3',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '145.01',
        base: '41.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '145.01',
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: true,
      },
      validatingAirlineCodes: ['AH'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '145.01',
            base: '41.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '3',
              cabin: 'ECONOMY',
              fareBasis: 'NFRDZOO',
              class: 'N',
              includedCheckedBags: {
                weight: 30,
                weightUnit: 'KG',
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '7',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-14',
      lastTicketingDateTime: '2023-09-14',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT2H10M',
          segments: [
            {
              departure: {
                iataCode: 'ORY',
                terminal: '4',
                at: '2023-09-15T14:00:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T15:10:00',
              },
              carrierCode: 'AH',
              number: '1011',
              aircraft: {
                code: '73H',
              },
              operating: {
                carrierCode: 'AH',
              },
              duration: 'PT2H10M',
              id: '4',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '145.01',
        base: '41.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '145.01',
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: true,
      },
      validatingAirlineCodes: ['AH'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '145.01',
            base: '41.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '4',
              cabin: 'ECONOMY',
              fareBasis: 'NFRDZOO',
              class: 'N',
              includedCheckedBags: {
                weight: 30,
                weightUnit: 'KG',
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '8',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-14',
      lastTicketingDateTime: '2023-09-14',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT2H10M',
          segments: [
            {
              departure: {
                iataCode: 'ORY',
                terminal: '4',
                at: '2023-09-15T16:55:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T18:05:00',
              },
              carrierCode: 'AH',
              number: '1005',
              aircraft: {
                code: '73H',
              },
              operating: {
                carrierCode: 'AH',
              },
              duration: 'PT2H10M',
              id: '5',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '145.01',
        base: '41.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '145.01',
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: true,
      },
      validatingAirlineCodes: ['AH'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '145.01',
            base: '41.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '5',
              cabin: 'ECONOMY',
              fareBasis: 'NFRDZOO',
              class: 'N',
              includedCheckedBags: {
                weight: 30,
                weightUnit: 'KG',
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '9',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-14',
      lastTicketingDateTime: '2023-09-14',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT2H10M',
          segments: [
            {
              departure: {
                iataCode: 'ORY',
                terminal: '4',
                at: '2023-09-15T20:10:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T21:20:00',
              },
              carrierCode: 'AH',
              number: '1007',
              aircraft: {
                code: '332',
              },
              operating: {
                carrierCode: 'AH',
              },
              duration: 'PT2H10M',
              id: '6',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '145.01',
        base: '41.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '145.01',
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: true,
      },
      validatingAirlineCodes: ['AH'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '145.01',
            base: '41.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '6',
              cabin: 'ECONOMY',
              fareBasis: 'NFRDZOO',
              class: 'N',
              includedCheckedBags: {
                weight: 30,
                weightUnit: 'KG',
              },
            },
          ],
        },
      ],
    },
    {
      type: 'flight-offer',
      id: '10',
      source: 'GDS',
      instantTicketingRequired: false,
      nonHomogeneous: false,
      oneWay: false,
      lastTicketingDate: '2023-09-14',
      lastTicketingDateTime: '2023-09-14',
      numberOfBookableSeats: 9,
      itineraries: [
        {
          duration: 'PT2H15M',
          segments: [
            {
              departure: {
                iataCode: 'CDG',
                terminal: '2D',
                at: '2023-09-15T09:00:00',
              },
              arrival: {
                iataCode: 'ALG',
                terminal: 'I',
                at: '2023-09-15T10:15:00',
              },
              carrierCode: 'AH',
              number: '1231',
              aircraft: {
                code: '73H',
              },
              operating: {
                carrierCode: 'AH',
              },
              duration: 'PT2H15M',
              id: '7',
              numberOfStops: 0,
              blacklistedInEU: false,
            },
          ],
        },
      ],
      price: {
        currency: 'EUR',
        total: '147.64',
        base: '41.00',
        fees: [
          {
            amount: '0.00',
            type: 'SUPPLIER',
          },
          {
            amount: '0.00',
            type: 'TICKETING',
          },
        ],
        grandTotal: '147.64',
      },
      pricingOptions: {
        fareType: ['PUBLISHED'],
        includedCheckedBagsOnly: true,
      },
      validatingAirlineCodes: ['AH'],
      travelerPricings: [
        {
          travelerId: '1',
          fareOption: 'STANDARD',
          travelerType: 'ADULT',
          price: {
            currency: 'EUR',
            total: '147.64',
            base: '41.00',
          },
          fareDetailsBySegment: [
            {
              segmentId: '7',
              cabin: 'ECONOMY',
              fareBasis: 'NFRDZOO',
              class: 'N',
              includedCheckedBags: {
                weight: 30,
                weightUnit: 'KG',
              },
            },
          ],
        },
      ],
    },
  ],
  dictionaries: {
    locations: {
      FCO: {
        cityCode: 'ROM',
        countryCode: 'IT',
      },
      CDG: {
        cityCode: 'PAR',
        countryCode: 'FR',
      },
      ORY: {
        cityCode: 'PAR',
        countryCode: 'FR',
      },
      ALG: {
        cityCode: 'ALG',
        countryCode: 'DZ',
      },
    },
    aircraft: {
      '320': 'AIRBUS A320',
      '332': 'AIRBUS A330-200',
      '32S': 'AIRBUS INDUSTRIE A318/A319/A320/A321',
      '73W': 'BOEING 737-700 (WINGLETS)',
      '73H': 'BOEING 737-800 (WINGLETS)',
    },
    currencies: {
      EUR: 'EURO',
    },
    carriers: {
      AH: 'AIR ALGERIE',
      AZ: 'ITA AIRWAYS',
      '5O': 'ASL AIRLINES FRANCE',
    },
  },
}
