// import JSZip from 'jszip'
import { saveAs } from 'file-saver'
import moment from 'moment'
import { ISO_DATE } from 'src/common/constants'
import * as XLSX from 'xlsx'

export enum FileFormat {
  XLSX = 'xlsx',
  JSON = 'json',
  // CSV = 'csv',
  TXT = 'txt',
}

class DownloaderService {
  async createfile(
    data: any[],
    format: FileFormat,
    title = 'download_' + moment.utc().format(ISO_DATE)
  ) {
    const fullTitleExtension = title + '.' + format
    let faltContent = ''
    switch (format) {
      case FileFormat.JSON: {
        faltContent = JSON.stringify(data)
        this.common(faltContent, fullTitleExtension)
        break
      }

      // case FileFormat.CSV: {
      //   faltContent = papaparse.unparse(data, { delimiter: '\t' })
      //   this.common(faltContent, fullTitleExtension)
      //   break
      // }

      case FileFormat.XLSX: {
        // const exportToCSV = () => {
        //   const ws = XLSX.utils.json_to_sheet(csvData)
        //   const wb = { Sheets: { data: ws }, SheetNames: ['data'] }
        //   const excelBuffer = XLSX.write(wb, {
        //     bookType: 'xlsx',
        //     type: 'array',
        //   })
        //   const data = new Blob([excelBuffer], { type: fileType })
        //   FileSaver.saveAs(data, fileName + fileExtension)
        // }

        try {
          const ws = XLSX.utils.json_to_sheet(data)
          const fits = fitToColumn(data)
          ws['!cols'] = fits
          const wb = { Sheets: { [title]: ws }, SheetNames: [title] }
          const excelBuffer = XLSX.write(wb, {
            bookType: 'xlsx',
            type: 'array',
          })
          const fileType =
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
          const payload = new Blob([excelBuffer], { type: fileType })
          saveAs(payload)
          break
        } catch (err) {
          console.log(`fail to create XLSX file`)
          console.log(err)
        }
        break
      }

      default:
        break
    }

    // get maximum character of each column
    function fitToColumn(arrOfObj: any[]) {
      const keys = Object.keys(arrOfObj[0])
      return keys.map((key) => {
        const max = arrOfObj
          .map((e) => ('' + e[key]).length)
          .sort((a, b) => b - a)[0]
        const keyLength = key.length
        const width = (max > keyLength ? max : keyLength) + 3
        return { width }
      })
    }
  }

  private common(faltContent: string, fullTitleExtension: string) {
    const element = document.createElement('a')
    const file = new Blob([faltContent], { type: 'text/plain' })
    element.href = URL.createObjectURL(file)
    element.download = fullTitleExtension
    document.body.appendChild(element)
    element.click()
  }

  // downloadMultiple(latexText: string) {
  //   const zip = new JSZip()

  //   const tab = [1, 2, 3]
  //   tab.forEach((elem, i) => {
  //     const content = `` + elem
  //     zip.file(`file_${i}.txt`, content)
  //   })

  //   this.SaveZip(zip)
  // }

  // private SaveZip(zip: JSZip) {
  //   zip.generateAsync({ type: 'blob' }).then(function (content) {
  //     saveAs(content, 'output.zip')
  //   })
  // }
}

export default new DownloaderService()
