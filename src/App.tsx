import { BrowserRouter, Routes, Route, Link } from 'react-router-dom'
import { SnackbarProvider } from 'notistack'
import { Alert, Grid, ThemeProvider } from '@mui/material'
import { Provider } from 'react-redux'
import { mTheme } from 'src/common/styles'
import { RouteI, routes } from 'src/routes'
import MDialog from 'src/components/MDialog'
import NotFound from 'src/components/NotFound'
import store from 'src/common/redux/store'
import MIcon from './components/MIcon'
import { APP_BASENAME, ENV_METADATA } from './common/constants'
import TopBar from './components/PageLayouts/TopBar'
import SideDrawer from './components/PageLayouts/SideDrawer'
import { useEffect, useState } from 'react'
import avionService from './services/amadeus/avion.service'

function App() {
  const [serverOn, setServerOn] = useState(true)

  useEffect(() => {
   
    // init()
    // const amadeus = new Amadeus({})

    // amadeus.referenceData.urls.checkinLinks.get({ airline: 'IB' })

    // Find the cheapest flights from SYD to BKK
    // amadeus.shopping.flightOffersSearch
    //   .get({
    //     originLocationCode: 'PAR',
    //     destinationLocationCode: 'MAD',
    //     departureDate: '2023-09-15',
    //     adults: '1',
    //   })
    //   .then(function (response: any) {
    //     console.log(response)
    //   })
    //   .catch(function (response: any) {
    //     console.error(response)
    //   })
  }, [])

  return (
    <Provider store={store}>
      <SnackbarProvider>
        <ThemeProvider theme={mTheme}>
          <BrowserRouter basename={APP_BASENAME}>
            <TopBar />
            <SideDrawer />
            <MDialog />
            <Alert
              severity="error"
              className="center-x"
              sx={{ display: serverOn ? 'none' : '', width: '100%' }}
            >
              <span>{`The connection to the server is not established. `}</span>
              <Link to="/contacts">Contact support</Link>
            </Alert>
            <Grid container className="center-x">
              <Grid
                item
                sx={{
                  position: `relative`,
                }}
                xs={10}
                // lg={5}
                className="center"
              >
                <Routes>
                  {routes.map((route: RouteI, i) => {
                    const Child = route.element
                    const Element = <Child />
                    return <Route key={i} path={route.path} element={Element} />
                  })}
                  <Route path={'*'} element={<NotFound />} />
                </Routes>
              </Grid>
            </Grid>
            <div
              style={{
                position: `fixed`,
                bottom: -5,
                right: 3,
                fontSize: `small`,
                margin: 2,
                opacity: 0.5,
              }}
              className="center-y"
            >
              <MIcon sx={{ m: 1 }} name={`ForkRight`} />
              {ENV_METADATA.branch}
              <MIcon sx={{ m: 1 }} name={`Sell`} />
              {ENV_METADATA.version}
            </div>
          </BrowserRouter>
        </ThemeProvider>
      </SnackbarProvider>
    </Provider>
  )
}

export default App
