import { APP_NAME } from './common/constants'
import About from './views/common/About/About'
import Contacts from './views/common/Contacts/Contacts'
import Setting from './views/common/Setting/Setting'
import Main from './views/Main/Main'

export enum Tags {
  TOP_MENU = 'TOP_MENU',
  SIDE_MENU = 'SIDE_MENU',
}

export interface RouteI {
  name: string
  path: string
  icon: string
  element: React.FunctionComponent
  tags: Tags[]
  description?: string
  image?: string
}

export const routes: RouteI[] = [
  {
    name: APP_NAME,
    path: `/`,
    icon: `TravelExplore`,
    element: Main,
    tags: [],
  },
  // others
  {
    name: `About`,
    path: `/about`,
    icon: `Info`,
    element: About,
    tags: [Tags.SIDE_MENU],
  },
  {
    name: `Contacts`,
    path: `/contacts`,
    icon: `ForwardToInbox`,
    element: Contacts,
    tags: [Tags.SIDE_MENU],
  },
  {
    name: `Setting`,
    path: `/setting`,
    icon: `Settings`,
    element: Setting,
    tags: [Tags.SIDE_MENU],
  },
]
